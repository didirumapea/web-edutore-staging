import * as firebase from 'firebase/app'
// import 'firebase/auth/dist/index.cjs'
// import 'firebase/firestore/dist/index.cjs'
// import 'firebase/database/dist/index.cjs'
import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/database'

if (!firebase.apps.length) {
  const config = {
    apiKey: "AIzaSyDxjrwMKZ-aAYljYPU1FvsdM-QNs7TXa-k",
    authDomain: "edutore01.firebaseapp.com",
    databaseURL: "https://edutore01.firebaseio.com",
    projectId: "edutore01",
    storageBucket: "edutore01.appspot.com",
    messagingSenderId: "926865736810"
  }
  firebase.initializeApp(config)
  // firebase.firestore().settings({timestampsInSnapshots: true})
}

export const auth = firebase.auth()
export const DB = firebase.database()
export default firebase

// const fireDb = firebase.firestore()
// export {fireDb}
// export {fireDb}
