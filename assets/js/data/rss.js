export default {
  data: [
    {
      "id": 2768,
      "date": "2019-09-30T09:56:26",
      "date_gmt": "2019-09-30T02:56:26",
      "guid": {
        "rendered": "https://edutore.com/news/?p=2768"
      },
      "modified": "2019-09-30T09:56:26",
      "modified_gmt": "2019-09-30T02:56:26",
      "slug": "perbedaan-jam-belajar-sekolah-di-negara-lain",
      "status": "publish",
      "type": "post",
      "link": "https://edutore.com/news/perbedaan-jam-belajar-sekolah-di-negara-lain/",
      "title": {
        "rendered": "Perbedaan Jam Belajar Sekolah di Negara Lain"
      },
      "content": {
        "rendered": "<p>Jam belajar sekolah di tiap negara tentu berbeda. Ada yang memiliki jumlah jam belajar paling lama dan paling sebentar. Jumlah jam belajar tersebut biasanya ditentukan berdasarkan kebutuhan dari sistem pendidikan yang telah ditetapkan oleh negara tersebut. Jam belajar di negara Indonesia sendiri rata-rata dimulai pukul 06.30 dan berakhir pukul 13.00 belum termasuk ekstrakurikuler atau belajar tambahan. Pelajar dengan jenjang lain juga memiliki jumlah jam belajar yang berbeda. Misalnya untuk jenjang sekolah dasar hanya lima jam, SMP enam jam, dan SMA tujuh hingga sembilan jam. Lalu bagaimana dengan negara lain? kita cari tahu yuk, Edufriends!</p>\n<p>&nbsp;</p>\n<p><strong>Finlandia</strong></p>\n<p><img class=\" wp-image-2769 aligncenter\" src=\"https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_finlandia-1-300x150.jpg\" alt=\"\" width=\"442\" height=\"221\" srcset=\"https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_finlandia-1-300x150.jpg 300w, https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_finlandia-1-600x300.jpg 600w, https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_finlandia-1-768x384.jpg 768w, https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_finlandia-1-1024x512.jpg 1024w\" sizes=\"(max-width: 442px) 100vw, 442px\" /></p>\n<p>Negara yang dikenal paling baik sistem pendidikannya ini ternyata hanya menetapkan waktu belajar di sekolah 4-5 jam dalam satu hari. Selain itu, siswa diberikan kesempatan istirahat 15-30 menit disela-sela pelajaran dengan tujuan otak bisa fokus secara optimal. Sedangkan untuk jenjang SMA siswa hanya datang pada jam pelajaran sesuai dengan kelas atau peminatan yang dipilih. Sistem ini berjalan baik karena didukung oleh masyarakat Finlandia yang mengutamakan pendidikan sebagai suatu kebutuhan.</p>\n<p><strong>Singapura</strong></p>\n<p style=\"text-align: left;\"><img class=\" wp-image-2771 aligncenter\" src=\"https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_singapura-1-300x150.jpg\" alt=\"\" width=\"460\" height=\"230\" srcset=\"https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_singapura-1-300x150.jpg 300w, https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_singapura-1-600x300.jpg 600w, https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_singapura-1-768x384.jpg 768w, https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_singapura-1-1024x512.jpg 1024w\" sizes=\"(max-width: 460px) 100vw, 460px\" /></p>\n<p style=\"text-align: left;\">Letak geografis negara Singapura sangatlah dekat dengan Indonesia. Namun, sistem pendidikannya sama nggak ya dengan Indonesia? Singapura menetapkan waktu belajar secara umum adalah 6-7 jam per hari. Pelajaran dimulai pukul 08.30 hingga pukul 16.30. Siswa di Singapura juga belajar lagi di rumah untuk mengerjakan PR ataupun ikut pelajaran tambahan di luar sekolah. Hm, nggak jauh beda dengan Indonesia ya, Edufriends.</p>\n<p><strong>Jepang</strong></p>\n<p><img class=\" wp-image-2772 aligncenter\" src=\"https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_jepang-1-300x150.jpg\" alt=\"\" width=\"454\" height=\"227\" srcset=\"https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_jepang-1-300x150.jpg 300w, https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_jepang-1-600x300.jpg 600w, https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_jepang-1-768x384.jpg 768w, https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_jepang-1-1024x512.jpg 1024w\" sizes=\"(max-width: 454px) 100vw, 454px\" /></p>\n<p>Jam belajar sekolah di negara Jepang umumnya dimulai pukul 09.00 hingga pukul 16.00. Setiap satu jam belajar, siswa diberikan istirahat selama 15 menit untuk merelaksasi otak dan otot agar dapat menerima pelajaran selanjutnya. Biasanya sepulang sekolah beberapa siswa mengikuti kelas tambahan atau les di luar sekolah, terutama siswa yang ingin mengikuti ujian masuk sekolah agar siap bersaing dengan siswa lainnya.</p>\n<p><strong>China</strong></p>\n<p><img class=\" wp-image-2773 aligncenter\" src=\"https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_china-2-300x150.jpg\" alt=\"\" width=\"442\" height=\"221\" srcset=\"https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_china-2-300x150.jpg 300w, https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_china-2-600x300.jpg 600w, https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_china-2-768x384.jpg 768w, https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_china-2-1024x512.jpg 1024w\" sizes=\"(max-width: 442px) 100vw, 442px\" /></p>\n<p>China merupakan salah satu negara yang sangat bersaing dalam hal pendidikan. Orang tua bisa memasukan anak ke berbagai kursus agar banak bisa masuk ke sekolah atau universitas ternama di negaranya. Rata-rata jam belajar sekolah di China adalah 6-7 jam sehari dan setelah siswa pulang sekolah juga mengerjakan PR.  Bahkan di hari Sabtu pun anak-anak disana belajar tambahan seperti les bahasa Inggris maupun les lainnya. Sistem pendidikan seperti ini <em>nggak</em> cuma diterapkan di jenjang SMA lho, bahkan sejak usia 6 tahun ke atas atau sejak dimulainya usia sekolah mereka menerapkan aturan ini.</p>\n<p><strong>Korea Selatan</strong></p>\n<p><img class=\" wp-image-2774 aligncenter\" src=\"https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_korsel-1-300x150.jpg\" alt=\"\" width=\"452\" height=\"226\" srcset=\"https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_korsel-1-300x150.jpg 300w, https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_korsel-1-600x300.jpg 600w, https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_korsel-1-768x384.jpg 768w, https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_korsel-1-1024x512.jpg 1024w\" sizes=\"(max-width: 452px) 100vw, 452px\" /></p>\n<p>Kamu pernah nonton drama Korea yang bertemakan sekolah? Kamu penasaran nggak sih kenapa ketika adegannya malam hari, mereka masih berseragam atau masih ada di sekolah? Ternyata, yang tergambar dalam drama Korea itu nggak jauh beda dengan kenyataannya, Edufriends. Rata-rata siswa di Korea mulai belajar di sekolah pukul 08.00-22.00. Jam belajar tersebut termasuk dengan belajar tambahan atau kursus di lembaga luar sekolah. Bahkan di hari libur mereka harus ke perpustakaan untuk belajar lagi. <em>Lho</em> kok lama banget? iya Edufriends, pendidikan di Korea sangat bersaing ketat. Mereka memiliki <em>mindset &#8220;harus nomor 1&#8221;</em>, itulah mengapa mereka sangat berusaha keras.</p>\n<p>Kebijakan pendidikan yang seperti ini dibentuk sedemikian rupa karena sudah disesuaikan dengan kebutuhan masyarakat dan kondisi negaranya, Edufriends. Dari fakta di atas, bisa disimpulkan bahwa jam belajar di Indonesia nggak terlalu lama dan nggak terlalu sebentar juga. Jadi, kamu harus bersyukur dan belajar giat supaya nggak kalah dengan anak-anak dari negara maju tadi. Menurutmu, sekolah yang seperti apa yang sesuai dengan kamu, Edufriends? <em>Share</em> pendapat kamu yuk!</p>\n<div style=\"text-align:left\" class=\"yasr-auto-insert-overall\"></div>",
        "protected": false
      },
      "excerpt": {
        "rendered": "<p>Jam belajar sekolah di tiap negara tentu berbeda. Ada yang memiliki jumlah jam belajar paling lama dan paling sebentar. Jumlah jam belajar tersebut biasanya ditentukan berdasarkan kebutuhan dari sistem pendidikan yang telah ditetapkan oleh negara tersebut. Jam belajar di negara &hellip; </p>\n",
        "protected": false
      },
      "author": 2,
      "featured_media": 2775,
      "comment_status": "closed",
      "ping_status": "closed",
      "sticky": false,
      "template": "",
      "format": "standard",
      "meta": {
        "pmpro_default_level": 0,
        "yasr_overall_rating": 0,
        "yasr_auto_insert_disabled": "",
        "yasr_review_type": "Other"
      },
      "categories": [
        34
      ],
      "tags": [
        142,
        135,
        191,
        136,
        140,
        190,
        141
      ],
      "modified_by": "Feby",
      "_links": {
        "self": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/2768"
          }
        ],
        "collection": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts"
          }
        ],
        "about": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/types/post"
          }
        ],
        "author": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/users/2"
          }
        ],
        "replies": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/comments?post=2768"
          }
        ],
        "version-history": [
          {
            "count": 1,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/2768/revisions"
          }
        ],
        "predecessor-version": [
          {
            "id": 2776,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/2768/revisions/2776"
          }
        ],
        "wp:featuredmedia": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/media/2775"
          }
        ],
        "wp:attachment": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/media?parent=2768"
          }
        ],
        "wp:term": [
          {
            "taxonomy": "category",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/categories?post=2768"
          },
          {
            "taxonomy": "post_tag",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/tags?post=2768"
          }
        ],
        "curies": [
          {
            "name": "wp",
            "href": "https://api.w.org/{rel}",
            "templated": true
          }
        ]
      }
    },
    {
      "id": 2693,
      "date": "2019-09-23T10:41:52",
      "date_gmt": "2019-09-23T03:41:52",
      "guid": {
        "rendered": "https://edutore.com/news/?p=2693"
      },
      "modified": "2019-09-23T10:41:52",
      "modified_gmt": "2019-09-23T03:41:52",
      "slug": "mengapa-kopi-instan-mudah-terbakar",
      "status": "publish",
      "type": "post",
      "link": "https://edutore.com/news/mengapa-kopi-instan-mudah-terbakar/",
      "title": {
        "rendered": "Mengapa Kopi Instan Mudah Terbakar?"
      },
      "content": {
        "rendered": "<p><span style=\"font-weight: 400;\">Edufriends, pernah mendengar atau melihat video tentang salah satu merek kopi kemasan yang membuat api membesar saat ditaburkan ke atas api?</span></p>\n<p><span style=\"font-weight: 400;\">Gara-gara video viral tersebut, kopi kemasan sempat diduga menggunakan bubuk mesiu dalam komposisi kopinya. Wah kira-kira benar <em>nggak</em> ya? Yuk, coba kita bahas satu persatu!</span></p>\n<p><span style=\"font-weight: 400;\">Oke, sebelum ke inti pembahasan, apa sih bubuk mesiu itu? </span><b>Bubuk mesiu</b><span style=\"font-weight: 400;\"> adalah bubuk hitam yang biasa digunakan sebagai bahan peledak cepat atau bahan pendorong ledakan senjata api atau kembang api. Bubuk mesiu terbuat dari campuran belerang, arang dan kalium nitrat.</span></p>\n<p><span style=\"font-weight: 400;\">Terus, apa s</span>aja kandungan pada kopi kemasan ini? Kopi kemasan terbuat dari kandungan gula, krimer nabati dan kopi bubuk instan. Kopi kemasan tersebut berbentuk bubuk yang ringan, mengandung minyak dan memiliki kadar air yang rendah alias kering.</p>\n<figure id=\"attachment_2200\" aria-describedby=\"caption-attachment-2200\" style=\"width: 488px\" class=\"wp-caption aligncenter\"><img class=\"wp-image-2200\" src=\"https://news.edutore.com/wp-content/uploads/2019/03/Kopi-Instant_01-300x150.jpg\" alt=\"\" width=\"488\" height=\"244\" srcset=\"https://news.edutore.com/wp-content/uploads/2019/03/Kopi-Instant_01-300x150.jpg 300w, https://news.edutore.com/wp-content/uploads/2019/03/Kopi-Instant_01-600x300.jpg 600w, https://news.edutore.com/wp-content/uploads/2019/03/Kopi-Instant_01-768x384.jpg 768w, https://news.edutore.com/wp-content/uploads/2019/03/Kopi-Instant_01-1024x512.jpg 1024w\" sizes=\"(max-width: 488px) 100vw, 488px\" /><figcaption id=\"caption-attachment-2200\" class=\"wp-caption-text\">                                              kandungan pada kopi instan</figcaption></figure>\n<p><span style=\"font-weight: 400;\">Lanjut! untuk mengetahui bagaimana bubuk kopi kemasan dapat membesarkan api, kamu harus paham tentang proses terjadinya api. Api terjadi karena adanya pertemuan 3</span> unsur yaitu, bahan bakar, oksigen dan panas. Biasanya proses ini disebut segitiga api.</p>\n<p><span style=\"font-weight: 400;\">Balik lagi ke kandungan bubuk kopi instan ini, saat eksperimen bubuk kopi dari kemasan yang ditabur di atas api, semua komponen yang memicu api membesar terpenuhi yaitu: 1) Panas api yang berasal dari korek, 2) Oksigen atau udara yang ada pada ruangan, 3) bahan bakar berupa bubuk kopi instan. Gula yang ada pada bubuk kopi mengandung karbohidrat , sedangkan krimer dan kopi instan mengandung lemak . Bahan-bahan tersebut kemudian </span><b>membentuk ikatan rantai karbon</b><span style=\"font-weight: 400;\">. BPOM juga telah menerangkan</span><i><span style=\"font-weight: 400;\"> lho,</span></i><span style=\"font-weight: 400;\"> tentang produk pangan yang memiliki ikatan rantai karbon, kadar air rendah, halus dan ringan, dapat terbakar jika disulut dengan api.</span></p>\n<figure id=\"attachment_2201\" aria-describedby=\"caption-attachment-2201\" style=\"width: 518px\" class=\"wp-caption aligncenter\"><img class=\"wp-image-2201\" src=\"https://news.edutore.com/wp-content/uploads/2019/03/Kopi-Instant_02-300x150.jpg\" alt=\"\" width=\"518\" height=\"259\" srcset=\"https://news.edutore.com/wp-content/uploads/2019/03/Kopi-Instant_02-300x150.jpg 300w, https://news.edutore.com/wp-content/uploads/2019/03/Kopi-Instant_02-600x300.jpg 600w, https://news.edutore.com/wp-content/uploads/2019/03/Kopi-Instant_02-768x384.jpg 768w, https://news.edutore.com/wp-content/uploads/2019/03/Kopi-Instant_02-1024x512.jpg 1024w\" sizes=\"(max-width: 518px) 100vw, 518px\" /><figcaption id=\"caption-attachment-2201\" class=\"wp-caption-text\">                               segitiga api yang dipicu kandungan pada kopi</figcaption></figure>\n<p><span style=\"font-weight: 400;\"><em>Nggak</em> hanya bubuk kopi kemasan ini aja yang bisa membesarkan api jika ditabur, tapi kopi jenis berbeda serta bahan-bahan lain yang memiliki karakteristik sama pun juga akan menghasilkan kejadian serupa. Misalnya tepung, merica bubuk, pati jagung, susu bubuk. Persamaan di antara mereka adalah karakteristiknya yang halus dan ringan. Fenomena ini biasa disebut </span><i><span style=\"font-weight: 400;\">dust explosion</span></i><span style=\"font-weight: 400;\"> yang artinya ledakan yang dipicu oleh debu atau partikel sehalus debu.</span></p>\n<div style=\"width: 640px;\" class=\"wp-video\"><!--[if lt IE 9]><script>document.createElement('video');</script><![endif]-->\n<video class=\"wp-video-shortcode\" id=\"video-2693-1\" width=\"640\" height=\"360\" preload=\"metadata\" controls=\"controls\"><source type=\"video/mp4\" src=\"https://news.edutore.com/wp-content/uploads/2019/09/Dust-Explosions-Cool-Science-Experiment.mp4?_=1\" /><a href=\"https://news.edutore.com/wp-content/uploads/2019/09/Dust-Explosions-Cool-Science-Experiment.mp4\">https://news.edutore.com/wp-content/uploads/2019/09/Dust-Explosions-Cool-Science-Experiment.mp4</a></video></div>\n<p>&nbsp;</p>\n<p><span style=\"font-weight: 400;\">Jadi, sekarang sudah jelas ya bahwa bubuk yang mudah terbakar bukan berarti mengandung bubuk mesiu di dalamnya, tetapi karena terdiri dari material-material yang memicu membesarnya api. Nah, biar kamu <em>nggak</em> terjebak hoax, Edufriends harus rajin-rajin membaca dan mencari tahu nih. Pantau terus edunews biar <em>nggak</em> ketinggalan bahan bacaan terbaru!</span></p>\n<div style=\"text-align:left\" class=\"yasr-auto-insert-overall\"></div>",
        "protected": false
      },
      "excerpt": {
        "rendered": "<p>Edufriends, pernah mendengar atau melihat video tentang salah satu merek kopi kemasan yang membuat api membesar saat ditaburkan ke atas api? Gara-gara video viral tersebut, kopi kemasan sempat diduga menggunakan bubuk mesiu dalam komposisi kopinya. Wah kira-kira benar nggak ya? &hellip; </p>\n",
        "protected": false
      },
      "author": 2,
      "featured_media": 2202,
      "comment_status": "closed",
      "ping_status": "closed",
      "sticky": false,
      "template": "",
      "format": "standard",
      "meta": {
        "pmpro_default_level": 0,
        "yasr_overall_rating": 0,
        "yasr_auto_insert_disabled": "",
        "yasr_review_type": "Other"
      },
      "categories": [
        36
      ],
      "tags": [
        44,
        179,
        180,
        45
      ],
      "modified_by": "Feby",
      "_links": {
        "self": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/2693"
          }
        ],
        "collection": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts"
          }
        ],
        "about": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/types/post"
          }
        ],
        "author": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/users/2"
          }
        ],
        "replies": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/comments?post=2693"
          }
        ],
        "version-history": [
          {
            "count": 2,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/2693/revisions"
          }
        ],
        "predecessor-version": [
          {
            "id": 2703,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/2693/revisions/2703"
          }
        ],
        "wp:featuredmedia": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/media/2202"
          }
        ],
        "wp:attachment": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/media?parent=2693"
          }
        ],
        "wp:term": [
          {
            "taxonomy": "category",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/categories?post=2693"
          },
          {
            "taxonomy": "post_tag",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/tags?post=2693"
          }
        ],
        "curies": [
          {
            "name": "wp",
            "href": "https://api.w.org/{rel}",
            "templated": true
          }
        ]
      }
    },
    {
      "id": 863,
      "date": "2019-09-09T09:09:25",
      "date_gmt": "2019-09-09T02:09:25",
      "guid": {
        "rendered": "https://edutore.com/news/?p=863"
      },
      "modified": "2019-09-09T10:22:19",
      "modified_gmt": "2019-09-09T03:22:19",
      "slug": "cara-memaksimalkan-diri-di-perkuliahan",
      "status": "publish",
      "type": "post",
      "link": "https://edutore.com/news/cara-memaksimalkan-diri-di-perkuliahan/",
      "title": {
        "rendered": "Cara Memaksimalkan Diri di Perkuliahan"
      },
      "content": {
        "rendered": "<p>Mahasiswa baru di beberapa universitas sudah mulai aktif perkuliahan. Bagaimana dengan kamu Edufriends? <em>Nggak</em> sedikit mahasiswa baru merasa bingung bagaimana beradaptasi dengan dunia perkuliahan yang jauh berbeda dengan sekolah. Menjadi mahasiswa artinya kita dituntut menjadi dewasa dengan menentukan masa depan sendiri, mengatur jadwal kuliah sendiri, dan membuat perencanaan akademik sendiri. Nah, buat kamu para mahasiswa baru, <em>nggak</em> perlu bingung lagi, Eduteam akan kasih tips bagaimana memaksimalkan diri di perkuliahan. Simak yuk!</p>\n<p><strong>Aktif Mencari Tahu</strong></p>\n<p>Saat berada di dunia perkuliahan, kita dituntut harus mandiri, termasuk aktif mencari tahu segala informasi yang ada di kampus. Informasi yang berkaitan dengan akademik maupun non akademik perlu dicari supaya Edufriends <em>nggak</em> ketinggalan info pentingnya. Kamu bisa tanya-tanya ke kakak tingkat, ke BEM kampus maupun ke Humas Kampus.</p>\n<p><strong>Membuat Rencana Akademik</strong></p>\n<p>Mumpung kamu masih di awal masa perkuliahan, ini waktu yang pas buat bikin rencana akademik. Misalnya membuat rencana tiap bulan yang berisi perkembangan akademik kamu, atau rencana semesteran yang berisi peningkatan nilai, kamu juga bisa menyusun target di tahun berapa kamu harus mulai <em>garap</em> skripsi dan lulus kuliah. Siap?</p>\n<p><strong>Membuat <em>To Do List</em></strong></p>\n<p>Selain membuat rencana akademik, Edufriends perlu membuat <em>to do list</em> dari keseluruhan kegiatan yang setiap hari Edufriends lakukan. Nanti jika kegiatan yang ada dalam <em>list</em> terselesaikan, tinggal dicoret atau diceklis aja. Hal ini biar semua kegiatan menjadi terarah dan jelas pencapaiannya.</p>\n<p><strong>Ikut Organisasi atau Komunitas</strong></p>\n<p>Jadwal kuliah yang kamu tentukan sendiri, tentu membuat waktumu menjadi lebih fleksibel. Daripada kamu hanya <em>luntang-lantung nggak</em> jelas, lebih baik waktu kosongmu digunakan untuk ikut organisasi atau komunitas yang kamu minati. Bertemu orang-orang baru dari latar belakang yang berbeda denganmu serta mengikuti diskusi forum akan memberikan pandangan baru serta memperluas wawasan kamu, Edufriends.</p>\n<p><strong><em>Time Management</em> itu Penting!</strong></p>\n<p>Semua yang udah disusun dan direncanakan <em>nggak</em> akan berjalan mulus tanpa <em>time management</em> yang baik. Edufriends harus pintar dalam mengelola waktu yang dipunya, jangan sampai semua yang udah disusun berantakan gitu aja. Kamu harus membagi waktumu secara seimbang pada catatan <em>to do list</em> yang kamu punya dan fokus padanya. Eits, biar kamu <em>nggak</em> merasa stress, jangan lupa menyisipkan <em>free time</em> buat relaksasi diri kamu sendiri ya!</p>\n<p>Yuk Edufriends mumpung masih awal perkuliahan, ini waktu yang pas buat kamu memulai hal yang baik. Supaya kedepannya perkuliahan kamu sedikit mudah dijalani. Semangat, Edufriends!</p>\n<div style=\"text-align:left\" class=\"yasr-auto-insert-overall\"></div>",
        "protected": false
      },
      "excerpt": {
        "rendered": "<p>Mahasiswa baru di beberapa universitas sudah mulai aktif perkuliahan. Bagaimana dengan kamu Edufriends? Nggak sedikit mahasiswa baru merasa bingung bagaimana beradaptasi dengan dunia perkuliahan yang jauh berbeda dengan sekolah. Menjadi mahasiswa artinya kita dituntut menjadi dewasa dengan menentukan masa depan &hellip; </p>\n",
        "protected": false
      },
      "author": 2,
      "featured_media": 1958,
      "comment_status": "closed",
      "ping_status": "closed",
      "sticky": false,
      "template": "",
      "format": "standard",
      "meta": {
        "pmpro_default_level": 0,
        "yasr_overall_rating": 0,
        "yasr_auto_insert_disabled": "",
        "yasr_review_type": "Other"
      },
      "categories": [
        40
      ],
      "tags": [
        102,
        82,
        103,
        101,
        100
      ],
      "modified_by": "Feby",
      "_links": {
        "self": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/863"
          }
        ],
        "collection": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts"
          }
        ],
        "about": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/types/post"
          }
        ],
        "author": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/users/2"
          }
        ],
        "replies": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/comments?post=863"
          }
        ],
        "version-history": [
          {
            "count": 2,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/863/revisions"
          }
        ],
        "predecessor-version": [
          {
            "id": 2126,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/863/revisions/2126"
          }
        ],
        "wp:featuredmedia": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/media/1958"
          }
        ],
        "wp:attachment": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/media?parent=863"
          }
        ],
        "wp:term": [
          {
            "taxonomy": "category",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/categories?post=863"
          },
          {
            "taxonomy": "post_tag",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/tags?post=863"
          }
        ],
        "curies": [
          {
            "name": "wp",
            "href": "https://api.w.org/{rel}",
            "templated": true
          }
        ]
      }
    },
    {
      "id": 1497,
      "date": "2019-09-03T08:51:19",
      "date_gmt": "2019-09-03T01:51:19",
      "guid": {
        "rendered": "https://edutore.com/news/?p=1497"
      },
      "modified": "2019-09-04T09:32:41",
      "modified_gmt": "2019-09-04T02:32:41",
      "slug": "mengapa-manusia-bermimpi-saat-tidur",
      "status": "publish",
      "type": "post",
      "link": "https://edutore.com/news/mengapa-manusia-bermimpi-saat-tidur/",
      "title": {
        "rendered": "Mengapa Manusia Bermimpi Saat Tidur?"
      },
      "content": {
        "rendered": "<p>Edufriends, apakah kamu pernah mengalami saat bangun tidur bantal kamu basah dan kamu berkeringat? atau bangun tidur dengan perasaan senang ditambah senyum-senyum sendiri? Pasti kamu habis mengalami yang namanya mimpi. Omong-omong tentang mimpi, kamu tahu <em>nggak</em> sih kenapa manusia bisa bermimpi?</p>\n<figure style=\"width: 303px\" class=\"wp-caption aligncenter\"><img class=\"\" src=\"https://media.giphy.com/media/8rEWYweSplFGF7Z8sd/source.gif\" width=\"303\" height=\"303\" /><figcaption class=\"wp-caption-text\">   mimpi lagi gowes sepeda (source: giphy.com)</figcaption></figure>\n<p>Menurut <strong>Sigmun Freud</strong>, mimpi merupakan manifestasi dari sebuah keinginan dari dalam diri kita yang belum terwujud. Proses mimpi itu terjadi secara natural di alam bawah sadar kita. Pernah <em>nggak</em> kamu lagi kepingin sesuatu, misal ingin banget beli PS4 dari sejak lama tapi belum bisa kebeli sampai kebawa mimpi kamu? Nah itu salah satu contoh sederhananya Edufriends. <em>Nggak</em> cuma itu aja, hal-hal lain yang kamu pikirkan secara berlarut-larut juga menjadi faktor terjadinya bermimpi ketika tidur.</p>\n<p>Otak bekerja berdasarkan perannya masing-masing. Saat tertidur otak akan menonaktifkan indera dan otot, namun otak <em>nggak</em> berhenti beraktivitas dan akan terus bekerja. Kamu masih ingat <em>nggak</em> Edufriends, bagian otak mana yang bertanggung jawab untuk berpikir, memahami, berbicara, kesadaran,  hingga mengambil keputusan? Nah ketika tidur, otak bagian <strong>korteks depan</strong> yang bertanggung jawab dengan itu semua berada pada mode nonaktif.  Karena Mimpi berkaitan dengan memori, kenangan dan emosi, bagian otak yang ikut terlibat adalah <strong><em>hippocampus</em></strong> yang terletak di bagian <em>lobus temporal</em> otak besar. Maka dari itu ketika bermimpi, kamu tetap bisa merasakan suatu kejadian yang terekam di otak kamu. Kejadian yang kamu alami ketika bermimpi pun sifatnya acak, bisa dari buah pikiran yang terjadi sejak lama maupun yang terjadi baru-baru ini.</p>\n<figure id=\"attachment_1950\" aria-describedby=\"caption-attachment-1950\" style=\"width: 300px\" class=\"wp-caption aligncenter\"><img class=\"wp-image-1950 size-medium\" src=\"https://news.edutore.com/wp-content/uploads/2019/09/HIPO-300x270.jpg\" alt=\"\" width=\"300\" height=\"270\" srcset=\"https://news.edutore.com/wp-content/uploads/2019/09/HIPO-300x270.jpg 300w, https://news.edutore.com/wp-content/uploads/2019/09/HIPO-600x541.jpg 600w, https://news.edutore.com/wp-content/uploads/2019/09/HIPO.jpg 689w\" sizes=\"(max-width: 300px) 100vw, 300px\" /><figcaption id=\"caption-attachment-1950\" class=\"wp-caption-text\">                      source: hellosehat.com</figcaption></figure>\n<p>Pada siklus tidur, ada empat tahap yang harus dilalui nih Edufriends, yaitu tahap tidur ringan (N1), tidur sedang (N2), tidur dalam (N3) dan tahap tidur nyenyak (REM). Manusia biasanya mengalami mimpi ketika berada pada tahap REM <em>(Rapid Eye Movement)</em>, tapi ada juga yang bermimpi meski masih berada tahap tidur ringan. Semakin dalam kamu tertidur, secara bertahap hippocampus mulai memutar kenangan-kenangan yang ada pada memorimu. Ketika kamu bermimpi saat berada di tahap tidur ringan, biasanya kamu <em>nggak</em> akan mengingat mimpi yang kamu rasakan. Sebaliknya jika kamu bermimpi saat berada pada tahap REM, kamu akan merasakan mimpi tersebut bahkan terkadang akan teringat dengan jelas.</p>\n<p>Wah, seru banget ya Edufriends mengetahui bagaimana bisa terjadi mimpi ketika kita tertidur. Bagi beberapa orang, bermimpi ketika tidur merupakan suatu hal yang ditunggu. Kalo Edufriends bagaimana? Apakah ada pengalaman menarik seputar mimpi? <em>Yuk</em>, <em>share</em> bareng Eduteam!</p>\n<div style=\"text-align:left\" class=\"yasr-auto-insert-overall\"></div>",
        "protected": false
      },
      "excerpt": {
        "rendered": "<p>Edufriends, apakah kamu pernah mengalami saat bangun tidur bantal kamu basah dan kamu berkeringat? atau bangun tidur dengan perasaan senang ditambah senyum-senyum sendiri? Pasti kamu habis mengalami yang namanya mimpi. Omong-omong tentang mimpi, kamu tahu nggak sih kenapa manusia bisa &hellip; </p>\n",
        "protected": false
      },
      "author": 2,
      "featured_media": 1939,
      "comment_status": "closed",
      "ping_status": "closed",
      "sticky": false,
      "template": "",
      "format": "standard",
      "meta": {
        "pmpro_default_level": 0,
        "yasr_overall_rating": 0,
        "yasr_auto_insert_disabled": "",
        "yasr_review_type": "Other"
      },
      "categories": [
        37,
        35
      ],
      "tags": [
        126,
        130,
        127,
        128,
        132,
        131,
        129
      ],
      "modified_by": "Feby",
      "_links": {
        "self": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/1497"
          }
        ],
        "collection": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts"
          }
        ],
        "about": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/types/post"
          }
        ],
        "author": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/users/2"
          }
        ],
        "replies": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/comments?post=1497"
          }
        ],
        "version-history": [
          {
            "count": 2,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/1497/revisions"
          }
        ],
        "predecessor-version": [
          {
            "id": 1982,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/1497/revisions/1982"
          }
        ],
        "wp:featuredmedia": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/media/1939"
          }
        ],
        "wp:attachment": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/media?parent=1497"
          }
        ],
        "wp:term": [
          {
            "taxonomy": "category",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/categories?post=1497"
          },
          {
            "taxonomy": "post_tag",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/tags?post=1497"
          }
        ],
        "curies": [
          {
            "name": "wp",
            "href": "https://api.w.org/{rel}",
            "templated": true
          }
        ]
      }
    },
    {
      "id": 855,
      "date": "2019-08-02T10:39:06",
      "date_gmt": "2019-08-02T03:39:06",
      "guid": {
        "rendered": "https://edutore.com/news/?p=855"
      },
      "modified": "2019-08-02T13:06:53",
      "modified_gmt": "2019-08-02T06:06:53",
      "slug": "pesan-kopi-dan-cari-tahu-karaktermu",
      "status": "publish",
      "type": "post",
      "link": "https://edutore.com/news/pesan-kopi-dan-cari-tahu-karaktermu/",
      "title": {
        "rendered": "Pesan Kopi dan Cari Tahu Karaktermu!"
      },
      "content": {
        "rendered": "<p>Edufriends, kamu pernah <em>nggak</em> belajar kelompok atau sekadar <em>hangout</em> bareng temen di kedai kopi? Semacam ritual yang <em>nggak</em> boleh terlewat, Eduteam kalo janjian sama teman pasti mampir ke <a href=\"https://www.instagram.com/coficozy/\"><em>Cofi</em></a> dan pesan kopi kesukaan Eduteam. Coba dong tebak, Eduteam suka kopi apa? Karena Eduteam ini manis dan lembut<i> *uhuk</i>, Eduteam suka banget kopi Latte. Nah, Edufriends tahu <em>nggak</em> sih bahwa kopi yang kamu pesan bisa mencerminkan karakter kamu? Mau tahu? Simak ya!</p>\n<p>&nbsp;</p>\n<p><strong>Capuccino</strong></p>\n<p>Kalo Edufriends suka banget dengan varian kopi capuccino, pasti kamu adalah pribadi yang optimis meski kadang sedikit sensitif. Kamu juga punya banyak teman karena sangat suka bersosialisasi dan berkumpul bareng kawan-kawan.</p>\n<p><strong>Kopi Hitam</strong></p>\n<p>Penikmat kopi hitam biasanya memiliki karakter yang keras kepala dan berterus terang. Iya, kamu juga nggak begitu suka bicara banyak, cenderung pendiam dan sedikit <em>moody</em>.</p>\n<p><strong>Frappucino</strong></p>\n<p>Disajikan dengan tampilan yang cantik, penikmat frappucino ini adalah seorang yang berjiwa muda. Kamu juga kerap menjadi <em>trendsetter </em>bagi orang lain. Pokoknya apa aja yang kamu lakukan dan kenakan pasti menarik perhatian orang banget deh. Meski begitu, kamu ini lebih sering melakukan sesuatu secara spontan.</p>\n<p><strong>Espresso</strong></p>\n<p>Kamu suka espresso? biar Eduteam tebak, pasti kamu adalah seseorang yang pekerja keras dan percaya diri. Karakter kamu yang seperti itulah yang membuat kamu mampu <em>multitasking</em> dengan baik. Kereeeen!</p>\n<p><strong>Latte</strong></p>\n<p>Kalo kamu penyuka latte, pasti kamu memiliki kepribadian yang manis dan lembut kayak Eduteam <em>*uhuk lagi. </em>Kamu juga merupakan seseorang yang perhatian dan suka menyenangkan orang lain. Tidak heran kalo kamu lebih sering menjaga teman atau keluarga kamu dibanding diri kamu sendiri.</p>\n<p>&nbsp;</p>\n<p>Seru banget kan mengetahui karakter kamu berdasarkan kopi yang kamu pesan? Sekarang kamu jadi bisa tahu karakter teman-teman kamu kalo pas lagi kumpul di <a href=\"https://www.instagram.com/coficozy/\"><em> Cofi</em></a> :p Kalo Edufriends, kopi mana yang paling kamu suka?</p>\n<p>&nbsp;</p>\n<div style=\"text-align:left\" class=\"yasr-auto-insert-overall\"></div>",
        "protected": false
      },
      "excerpt": {
        "rendered": "<p>Edufriends, kamu pernah nggak belajar kelompok atau sekadar hangout bareng temen di kedai kopi? Semacam ritual yang nggak boleh terlewat, Eduteam kalo janjian sama teman pasti mampir ke Cofi dan pesan kopi kesukaan Eduteam. Coba dong tebak, Eduteam suka kopi &hellip; </p>\n",
        "protected": false
      },
      "author": 2,
      "featured_media": 856,
      "comment_status": "closed",
      "ping_status": "closed",
      "sticky": false,
      "template": "",
      "format": "standard",
      "meta": {
        "pmpro_default_level": 0,
        "yasr_overall_rating": 0,
        "yasr_auto_insert_disabled": "",
        "yasr_review_type": "Other"
      },
      "categories": [
        34
      ],
      "tags": [
        94,
        97,
        43,
        44,
        98,
        99
      ],
      "modified_by": "Feby",
      "_links": {
        "self": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/855"
          }
        ],
        "collection": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts"
          }
        ],
        "about": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/types/post"
          }
        ],
        "author": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/users/2"
          }
        ],
        "replies": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/comments?post=855"
          }
        ],
        "version-history": [
          {
            "count": 2,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/855/revisions"
          }
        ],
        "predecessor-version": [
          {
            "id": 861,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/855/revisions/861"
          }
        ],
        "wp:featuredmedia": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/media/856"
          }
        ],
        "wp:attachment": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/media?parent=855"
          }
        ],
        "wp:term": [
          {
            "taxonomy": "category",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/categories?post=855"
          },
          {
            "taxonomy": "post_tag",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/tags?post=855"
          }
        ],
        "curies": [
          {
            "name": "wp",
            "href": "https://api.w.org/{rel}",
            "templated": true
          }
        ]
      }
    },
    {
      "id": 852,
      "date": "2019-08-01T17:12:28",
      "date_gmt": "2019-08-01T10:12:28",
      "guid": {
        "rendered": "https://edutore.com/news/?p=852"
      },
      "modified": "2019-08-02T08:28:38",
      "modified_gmt": "2019-08-02T01:28:38",
      "slug": "mengetahui-7-manfaat-kopi-bagi-wajah",
      "status": "publish",
      "type": "post",
      "link": "https://edutore.com/news/mengetahui-7-manfaat-kopi-bagi-wajah/",
      "title": {
        "rendered": "Mengetahui 7 Manfaat Kopi Bagi Wajah"
      },
      "content": {
        "rendered": "<p>Kopi menjadi salah satu minuman yang digemari oleh banyak orang. Aroma kopi yang khas dan rasanya yang nikmat membuat kopi <em>nggak</em> hanya digemari oleh kaum pria saja tetapi kaum wanita pun kini banyak yang menyukai kopi. Eduteam sih suka banget ngopi di pagi hari di <a href=\"https://www.instagram.com/coficozy/\"><em>Cofi</em></a>. Selain untuk diminum, ternyata kopi juga memiliki segudang manfaat untuk perawatan wajah, <em>lho</em> Edufriends. Wah gimana maksudnya tuh?</p>\n<p>Tenang, tenang.. untuk melakukan perawatan wajah menggunakan kopi, Edufriends <em>nggak</em> perlu sampai cuci muka pake kopi kok! kalo begitu sih bukannya merawat wajah, yang ada malah melepuh karena kepanasan 😀</p>\n<p>Bubuk kopi  menyimpan berbagai nutrisi seperti <strong>karbohidrat, glikosida, mineral, asam amino, protein, kafein, asam karboksilat, trigoneline, dan asam klorogenat</strong> yang bermanfaat bagi kulit jika dijadikan masker. Edufriends bisa mencampurkan bubuk kopi murni dengan air, madu, lemon atau lidah buaya dan menggunakannya sebagai masker. Nah, apa aja sih manfaat menggunakan masker kopi? Simak <em>kuy</em>!</p>\n<p>1. Mengangkat Sel Kulit Mati</p>\n<p>Secara alami, kulit memiliki kemampuan untuk regenarasi. Kulit mati yang ada di wajah harus segera dibersihkan supaya <em>nggak</em> menyebabkan kulit kusam dan membentuk flek hitam pada wajah. Masker kopi mampu mengeksfoliate kulit serta merawat kulit menjadi lebih lembut.</p>\n<p>2. Melembutkan dan Menghaluskan Kulit</p>\n<p>Menggunakan masker kopi  dapat membuat kulit menjadi lembut dan halus<em> lho</em> Edufriends. Kamu bisa melihat hasilnya jika menggunakannya secara rutin seminggu dua kali atau selama waktu yang diperlukan.</p>\n<p>3. Menyamarkan Lingkaran Hitam di Bawah Mata</p>\n<p>Edufriends, <em>bete</em> banget <em>nggak</em> sih kalo ada lingkaran hitam di bawah mata? <em>ugh</em>, bikin penampilan kita terlihat nggak segar. Edufriends bisa menggunakan masker kopi dan membalurnya di bawah mata secara rutin dua hingga tiga minggu sekali untuk hasil maksimal.</p>\n<p>4. Menyamarkan Bekas Jerawat</p>\n<p>Kandungan kafein pada kopi nyatanya dapat mengurangi efek kemerahan bekas jerawat pada wajah. Kopi juga berfungsi sebagai diuretik yang membantu melancarkan sirkulasi pembuluh darah. Kandungan lain pada kopi juga membantu mengatasi masalah kulit pada wajah.</p>\n<p>5. Mengecilkan Pori-pori</p>\n<p>Menggunakan masker kopi dapat mengurangi kandungan minyak pada wajah. Kandungan minyak yang seimbang dapat membuat pori-pori pada kulit wajah terlihat lebih kecil.</p>\n<p>6. Bekerja Sebagai Anti-oksidan</p>\n<p>Kandungan plavanoid dan polifenol pada kopi berfungsi sebagai anti oksidan yang dapat mengurangi masalah kulit akibat paparan sinar matahari.</p>\n<p>7. Mengencangkan Kulit Wajah</p>\n<p>Kandungan-kandungan bermanfaat pada kopi dapat memberikan nutrisi tambahan bagi kulit sehingga kulit menjadi lebih kencang dan terawat. <em>Bye bye</em> penuaan dini~</p>\n<p>Nah itulah beberapa manfaat kopi bagi wajah. Penggunaan masker kopi secara rutin dapat mengurangi masalah pada kulit wajah serta mencegah kerusakan pada kulit. Bagaimana Edufriends, tertarik untuk menggunakan masker kopi? Eduteam mau maskeran dulu ah, biar nanti sore bisa ngopi cantik di <a href=\"https://www.instagram.com/coficozy/\"><em>Cofi</em></a>!</p>\n<div style=\"text-align:left\" class=\"yasr-auto-insert-overall\"></div>",
        "protected": false
      },
      "excerpt": {
        "rendered": "<p>Kopi menjadi salah satu minuman yang digemari oleh banyak orang. Aroma kopi yang khas dan rasanya yang nikmat membuat kopi nggak hanya digemari oleh kaum pria saja tetapi kaum wanita pun kini banyak yang menyukai kopi. Eduteam sih suka banget &hellip; </p>\n",
        "protected": false
      },
      "author": 2,
      "featured_media": 833,
      "comment_status": "closed",
      "ping_status": "closed",
      "sticky": false,
      "template": "",
      "format": "standard",
      "meta": {
        "pmpro_default_level": 0,
        "yasr_overall_rating": 0,
        "yasr_auto_insert_disabled": "",
        "yasr_review_type": "Other"
      },
      "categories": [
        35,
        40
      ],
      "tags": [
        95,
        94,
        44,
        96,
        27
      ],
      "modified_by": "Feby",
      "_links": {
        "self": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/852"
          }
        ],
        "collection": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts"
          }
        ],
        "about": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/types/post"
          }
        ],
        "author": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/users/2"
          }
        ],
        "replies": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/comments?post=852"
          }
        ],
        "version-history": [
          {
            "count": 1,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/852/revisions"
          }
        ],
        "predecessor-version": [
          {
            "id": 853,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/852/revisions/853"
          }
        ],
        "wp:featuredmedia": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/media/833"
          }
        ],
        "wp:attachment": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/media?parent=852"
          }
        ],
        "wp:term": [
          {
            "taxonomy": "category",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/categories?post=852"
          },
          {
            "taxonomy": "post_tag",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/tags?post=852"
          }
        ],
        "curies": [
          {
            "name": "wp",
            "href": "https://api.w.org/{rel}",
            "templated": true
          }
        ]
      }
    },
    {
      "id": 823,
      "date": "2019-07-23T15:58:50",
      "date_gmt": "2019-07-23T08:58:50",
      "guid": {
        "rendered": "https://edutore.com/news/?p=823"
      },
      "modified": "2019-07-31T10:04:13",
      "modified_gmt": "2019-07-31T03:04:13",
      "slug": "garansi-lulus-ujian-dari-edutore",
      "status": "publish",
      "type": "post",
      "link": "https://edutore.com/news/garansi-lulus-ujian-dari-edutore/",
      "title": {
        "rendered": "Garansi Lulus Ujian dari Edutore"
      },
      "content": {
        "rendered": "<p>Sudah  belajar siang dan malam tapi masih belum tenang menghadapi ujian?</p>\n<p>Ada solusi nih buat kamu yang butuh ketenangan menuju ujian.<em> Yupp</em> Garansi Lulus Ujian dari Edutore solusinya!</p>\n<p>Apa itu Garansi Lulus Ujian?? Yuk simak ulasannya!</p>\n<p>&nbsp;</p>\n<p>Program Garansi Lulus Ujian adalah program dari Edutore yang memastikan kamu akan mendapatkan 100% uang berlangganan kembali jika kamu tidak naik kelas atau tidak  lulus UN.</p>\n<p>Syarat dan Ketentuan promo Garansi Lulus Ujian:</p>\n<ol>\n<li>Promo berlaku khusus untuk pelajar SD, SMP, dan SMA</li>\n<li>Promo berlaku untuk pembelian langganan 12 bulan</li>\n<li>Jaminan uang kembali 100% jika tidak naik kelas atau tidak lulus Ujian Nasional</li>\n<li>Hanya berlaku 1 x klaim untuk 1 akun.</li>\n<li>Paket langganan tidak dapat di hentikan sebelum periode berlangganan habis</li>\n</ol>\n<p>&nbsp;</p>\n<p>Syarat pengajuan klaimnya juga mudah, kamu hanya perlu :</p>\n<ol>\n<li>Siapkan dokumen berikut:</li>\n</ol>\n<ul>\n<li>Rapot semester akhir yang sudah di legalisir oleh pihak sekolah</li>\n<li>Fotocopy halaman dean rapot.</li>\n<li>Surat keterangan tidak lulus yang sudah di legalisir oleh pihak sekolah (untuk ujian nasional)</li>\n<li>Fotocopy Buku tabungan</li>\n<li>File PDF email invoice paket berlangganan</li>\n</ul>\n<p>Kirimkan dokumen tersebut dalam file pdf kirim ke <a href=\"mailto:info@edutore.com\">info@edutore.com</a> *size file tidak boleh lebih dari 3 MB. Gambar harus jelas terbaca &amp; tidak ada bagian yang terpotong.</p>\n<ol start=\"2\">\n<li>Proses verifikasi data di lakukan 3 x 24 jam</li>\n<li>Edutore akan melakukan pembayaran klaim maksimal 30 hari kerja sejak klaim di setujui</li>\n<li>Pembayaran klaim akan di transfer langsung ke rekening yang tertera pada foto buku tabungan</li>\n<li>Belajar minimal 5 jam seminggu di platform Edutore</li>\n<li>Data diri pelajar dan orang tua/wali di aplikasi/web Edutore terisi lengkap</li>\n<li>Data diri pelajar sesuai dengan dokumen yang disertakan</li>\n</ol>\n<p>&nbsp;</p>\n<p>Setelah belajar di Edutore kamu akan merasakan pengalaman belajar yang menyenangkan dan menenangkan. <em>Eits</em>, periodenya terbatas loh, jadi jangan sampai ketinggalan ya. Yuk, langganan sekarang!</p>\n<div style=\"text-align:left\" class=\"yasr-auto-insert-overall\"></div>",
        "protected": false
      },
      "excerpt": {
        "rendered": "<p>Sudah  belajar siang dan malam tapi masih belum tenang menghadapi ujian? Ada solusi nih buat kamu yang butuh ketenangan menuju ujian. Yupp Garansi Lulus Ujian dari Edutore solusinya! Apa itu Garansi Lulus Ujian?? Yuk simak ulasannya! &nbsp; Program Garansi Lulus &hellip; </p>\n",
        "protected": false
      },
      "author": 2,
      "featured_media": 824,
      "comment_status": "closed",
      "ping_status": "closed",
      "sticky": false,
      "template": "",
      "format": "standard",
      "meta": {
        "pmpro_default_level": 0,
        "yasr_overall_rating": 0,
        "yasr_auto_insert_disabled": "",
        "yasr_review_type": "Other"
      },
      "categories": [
        33
      ],
      "tags": [
        88,
        89,
        87,
        78,
        75,
        48,
        76
      ],
      "modified_by": "Feby",
      "_links": {
        "self": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/823"
          }
        ],
        "collection": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts"
          }
        ],
        "about": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/types/post"
          }
        ],
        "author": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/users/2"
          }
        ],
        "replies": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/comments?post=823"
          }
        ],
        "version-history": [
          {
            "count": 2,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/823/revisions"
          }
        ],
        "predecessor-version": [
          {
            "id": 830,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/823/revisions/830"
          }
        ],
        "wp:featuredmedia": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/media/824"
          }
        ],
        "wp:attachment": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/media?parent=823"
          }
        ],
        "wp:term": [
          {
            "taxonomy": "category",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/categories?post=823"
          },
          {
            "taxonomy": "post_tag",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/tags?post=823"
          }
        ],
        "curies": [
          {
            "name": "wp",
            "href": "https://api.w.org/{rel}",
            "templated": true
          }
        ]
      }
    },
    {
      "id": 820,
      "date": "2019-07-23T15:58:05",
      "date_gmt": "2019-07-23T08:58:05",
      "guid": {
        "rendered": "https://edutore.com/news/?p=820"
      },
      "modified": "2019-07-31T08:56:39",
      "modified_gmt": "2019-07-31T01:56:39",
      "slug": "paket-berlangganan-edutore",
      "status": "publish",
      "type": "post",
      "link": "https://edutore.com/news/paket-berlangganan-edutore/",
      "title": {
        "rendered": "Pilihan Paket Berlangganan Edutore"
      },
      "content": {
        "rendered": "<p>Halo Edufriends, tahun ajaran baru sudah dimulai, apakah kebutuhan belajar kamu di semester ini sudah terpenuhi? Edutore punya penawaran menarik untuk memenuhi kebutuhan belajar kamu nih. Sekarang  kamu bisa mengakses buku buku di Edutore dengan membeli satuan atau dengan  paket berlangganan loh,  ada 4 pilihan paket berlangganan yang dapat menunjang kebutuhan belajar kamu, berikut pilihan langganannya:</p>\n<p>&nbsp;</p>\n<p><img class=\"wp-image-826 aligncenter\" src=\"https://news.edutore.com/wp-content/uploads/2019/07/190723-Edutopics-Garansi-Lulus-Ujian_Image1-1.jpg\" alt=\"\" width=\"887\" height=\"524\" srcset=\"https://news.edutore.com/wp-content/uploads/2019/07/190723-Edutopics-Garansi-Lulus-Ujian_Image1-1.jpg 1405w, https://news.edutore.com/wp-content/uploads/2019/07/190723-Edutopics-Garansi-Lulus-Ujian_Image1-1-600x354.jpg 600w, https://news.edutore.com/wp-content/uploads/2019/07/190723-Edutopics-Garansi-Lulus-Ujian_Image1-1-300x177.jpg 300w, https://news.edutore.com/wp-content/uploads/2019/07/190723-Edutopics-Garansi-Lulus-Ujian_Image1-1-768x454.jpg 768w, https://news.edutore.com/wp-content/uploads/2019/07/190723-Edutopics-Garansi-Lulus-Ujian_Image1-1-1024x605.jpg 1024w\" sizes=\"(max-width: 887px) 100vw, 887px\" /></p>\n<p>Dengan berlangganan kamu akan mendapatkan Keuntungan:</p>\n<ul>\n<li>Dapat mengakses ratusan buku soal dengan lebih dari 100.000 butir soal dari penerbit unggulan.</li>\n<li>Semakin panjang periode berlangganan yang di beli, kamu akan mendapat keuntungan harga terbaik karena kedepannya edutore akan menambah jumlah buku soal.</li>\n<li>Harga buku soalnya menjadi jauh lebih murah jika berlangganan.</li>\n<li>Bonus garansi lulus untuk paket berlangganan 1 tahun</li>\n</ul>\n<p>Cara berlangganannya cukup mudah, :</p>\n<ul>\n<li>Masuk ke platform Edutore</li>\n<li>Sign up</li>\n<li>Masuk ke langganan saya, dan pilih paket berlangganan</li>\n<li>Klik detail dan pilih pembayaran online</li>\n<li>Pilih penyedia layanan</li>\n<li>Lihat nomor rekening</li>\n<li>lakukan pembayaran ke rekening yang tertera</li>\n<li>selesaikan pembayaran</li>\n</ul>\n<p>&nbsp;</p>\n<p>Gimana Edufriends?  Sangat bermanfaat untuk kebutuhan belajar kamu kan? Dengan langganan kamu akan siap untuk lebih baik lagi di semester ini. <em>Eits</em>, periodenya terbatas loh, jadi jangan sampai ketinggalan ya. Yuk, langganan sekarang!</p>\n<p>&nbsp;</p>\n<div style=\"text-align:left\" class=\"yasr-auto-insert-overall\"></div>",
        "protected": false
      },
      "excerpt": {
        "rendered": "<p>Halo Edufriends, tahun ajaran baru sudah dimulai, apakah kebutuhan belajar kamu di semester ini sudah terpenuhi? Edutore punya penawaran menarik untuk memenuhi kebutuhan belajar kamu nih. Sekarang  kamu bisa mengakses buku buku di Edutore dengan membeli satuan atau dengan  paket &hellip; </p>\n",
        "protected": false
      },
      "author": 2,
      "featured_media": 837,
      "comment_status": "closed",
      "ping_status": "closed",
      "sticky": false,
      "template": "",
      "format": "standard",
      "meta": {
        "pmpro_default_level": 0,
        "yasr_overall_rating": 0,
        "yasr_auto_insert_disabled": "",
        "yasr_review_type": "Other"
      },
      "categories": [
        33
      ],
      "tags": [
        83,
        86,
        85,
        84
      ],
      "modified_by": "Feby",
      "_links": {
        "self": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/820"
          }
        ],
        "collection": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts"
          }
        ],
        "about": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/types/post"
          }
        ],
        "author": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/users/2"
          }
        ],
        "replies": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/comments?post=820"
          }
        ],
        "version-history": [
          {
            "count": 2,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/820/revisions"
          }
        ],
        "predecessor-version": [
          {
            "id": 838,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/820/revisions/838"
          }
        ],
        "wp:featuredmedia": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/media/837"
          }
        ],
        "wp:attachment": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/media?parent=820"
          }
        ],
        "wp:term": [
          {
            "taxonomy": "category",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/categories?post=820"
          },
          {
            "taxonomy": "post_tag",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/tags?post=820"
          }
        ],
        "curies": [
          {
            "name": "wp",
            "href": "https://api.w.org/{rel}",
            "templated": true
          }
        ]
      }
    },
    {
      "id": 716,
      "date": "2019-06-28T11:16:50",
      "date_gmt": "2019-06-28T04:16:50",
      "guid": {
        "rendered": "https://edutore.com/news/news/?p=716"
      },
      "modified": "2019-07-23T16:46:56",
      "modified_gmt": "2019-07-23T09:46:56",
      "slug": "mengenal-sistem-zonasi-setujukah-kamu",
      "status": "publish",
      "type": "post",
      "link": "https://edutore.com/news/mengenal-sistem-zonasi-setujukah-kamu/",
      "title": {
        "rendered": "Mengenal Sistem Zonasi. Setujukah Kamu?"
      },
      "content": {
        "rendered": "\r\n<p>Pendaftaran Penerimaan Peserta Didik Baru (PPDB) telah dibuka. Kementerian Pendidikan dan Kebudayaan menetapkan aturan baru berupa sistem zonasi. Pemberlakuan sistem zonasi ini sempat bikin heboh pelajar juga orang tua murid. <em>Lho</em> emang ada apa sih dengan sistem zonasi?</p>\r\n\r\n\r\n\r\n<p>Sebelum kita bahas bareng-bareng tentang sistem zonasi, Edufriends tahu <em>nggak</em> nih apa artinya sistem zonasi ini?</p>\r\n\r\n\r\n\r\n<p><em>Yoi</em>, Sistem zonasi merupakan sistem penerimaan murid baru yang <em>nggak</em> cuma melihat dari nilai si calon muridnya aja, tetapi juga melihat dari jarak tempat tinggal calon murid dengan sekolah. Calon murid yang jarak rumahnya dekat dengan sekolah dianggap lebih berhak dan prioritas untuk diterima di sekolah tersebut.</p>\r\n\r\n\r\n\r\n<p><strong>Berdasarkan permendikbud nomor 51 tahun 2019</strong>, penerimaan murid baru dilakukan melalui tiga jalur, yaitu <strong>jarak tempuh rumah-sekolah</strong> dengan kuota minimal 90 persen, <strong>jalur prestasi</strong> dengan kuota maksimal 5 persen dan<strong> jalur perpindahan</strong> orang tua dengan kuota 5 persen.</p>\r\n\r\n\r\n\r\n<p>&nbsp;</p>\r\n\r\n\r\n\r\n<div class=\"wp-block-image\">\r\n<figure class=\"aligncenter is-resized\"><img class=\"wp-image-739\" src=\"https://edutore.com/news/news/wp-content/uploads/2019/06/artikel-02-1-1024x855.png\" alt=\"\" width=\"452\" height=\"377\" srcset=\"https://news.edutore.com/wp-content/uploads/2019/06/artikel-02-1-1024x855.png 1024w, https://news.edutore.com/wp-content/uploads/2019/06/artikel-02-1-600x501.png 600w, https://news.edutore.com/wp-content/uploads/2019/06/artikel-02-1-300x251.png 300w, https://news.edutore.com/wp-content/uploads/2019/06/artikel-02-1-768x642.png 768w\" sizes=\"(max-width: 452px) 100vw, 452px\" /></figure>\r\n</div>\r\n\r\n\r\n\r\n<p>&nbsp;</p>\r\n\r\n\r\n\r\n<p><em>Lalu, seberapa jauh batas radius tempat tinggal yang seharusnya?</em></p>\r\n\r\n\r\n\r\n<p>Meski jarak tempuh rumah ke sekolah menjadi penentu, hal demikian juga dibatasi oleh provinsi. Seandainya jarak rumah kamu dekat dengan sekolah, tetapi jika beda provinsi akan berbeda lagi ketentuannya.</p>\r\n\r\n\r\n\r\n<p>Misalnya nih, kamu warga Bekasi ingin mendaftar ke sekolah di Jakarta Timur. Kalo berdasarkan jarak sih lebih deket kamu dibanding orang lain yang rumahnya di Jakarta Selatan meski dia sama-sama provinsi DKI Jakarta. Nah, jika terjadi hal demikian maka ketentuannya berbeda lagi berdasarkan peraturan yang tertulis di masing-masing provinsi.</p>\r\n\r\n\r\n\r\n<p><em>Trus, kalo jarak sekolah kamu dengan pendaftar lainnya sama, bagaimana sekolah menentukannya?</em></p>\r\n\r\n\r\n\r\n<p>Hal ini memang <em>nggak</em> bisa terhindarkan, Edufriends. Nah, sekolah menentukannya dengan cara melihat nilai UN dan melihat siapa yang lebih dulu mendaftar.</p>\r\n\r\n\r\n\r\n<p>Sistem zonasi ini menimbulkan polemik baru. Ada yang setuju-setuju aja tapi lebih banyak yang <em>nggak</em> setuju. Emangnya apa sih tujuan diberlakukan sistem zonasi ini?</p>\r\n\r\n\r\n\r\n<p><strong>1. Pemerataan Kualitas Pendidikan</strong></p>\r\n\r\n\r\n\r\n<p>Sistem zonasi ini diharapkan <em>nggak</em> ada lagi stigma sekolah yang lebih unggul. <strong>Dari segi siswa</strong>, siswa-siswi yang memiliki capaian akademik yang tinggi bisa tersebar di sekolah sesuai zonasi. Sehingga dalam sekolah tersebut nantinya terdiri dari siswa yang memiliki capaian akademik yang beragam dan bisa bermanfaat satu sama lain. Sedangkan <strong>dari segi</strong> <strong>tenaga pendidik </strong>adalah agar tenaga pendidik mampu meningkatkan kreativitas dalam kelas yang berisi murid dengan kemampuan akademis yang beragam.</p>\r\n\r\n\r\n\r\n<p><strong>2. Mencegah Penumpukan di Satu Wilayah</strong></p>\r\n\r\n\r\n\r\n<p>Berkaitan dengan pemerataan juga nih Edufriends, biasanya siswa yang memiliki nilai UN yang bagus ingin memilih sekolah yang favorit namun letaknya berada di luar zonasi. Otomatis sekolah favorit hanya terisi siswa yang memiliki nilai bagus dan unggulan aja. Hal demikian dianggap sulit untuk menganalisis kebutuhan dan pendistribusian tenaga pendidik demi kemajuan pendidikan Indonesia.</p>\r\n\r\n\r\n\r\n<p><strong>3. Lingkungan Sekolah Menjadi Lebih Dekat</strong></p>\r\n\r\n\r\n\r\n<p>Jarak tempuh sekolah yang dekat dari rumah, dianggap sebagai cara efektif untuk mengurangi tingkat stress pada siswa akibat kelelahan di jalan. Waktu yang tersimpan pun bisa dimanfaatkan oleh siswa untuk mengisi kegiatan yang lebih produktif.</p>\r\n\r\n\r\n\r\n<p>&nbsp;</p>\r\n\r\n\r\n\r\n<div class=\"wp-block-image\">\r\n<figure class=\"aligncenter is-resized\"><img class=\"wp-image-734\" src=\"https://edutore.com/news/news/wp-content/uploads/2019/06/artikel-01-1024x447.png\" alt=\"\" width=\"666\" height=\"290\" srcset=\"https://news.edutore.com/wp-content/uploads/2019/06/artikel-01-1024x447.png 1024w, https://news.edutore.com/wp-content/uploads/2019/06/artikel-01-600x262.png 600w, https://news.edutore.com/wp-content/uploads/2019/06/artikel-01-300x131.png 300w, https://news.edutore.com/wp-content/uploads/2019/06/artikel-01-768x336.png 768w\" sizes=\"(max-width: 666px) 100vw, 666px\" />\r\n<figcaption>Tujuan Sistem Zonasi Sekolah</figcaption>\r\n</figure>\r\n</div>\r\n\r\n\r\n\r\n<p>Sistem zonasi ini sebenarnya sudah lama dilakukan namun belum masif pelaksanaannya. Sistem ini merupakan penyesuaian dan pengembangan dari kebijakan rayonisasi. Banyak yang merasa terbatasi dengan adanya sistem zonasi ini karena <em>nggak</em> bisa memilih sekolah idamannya.</p>\r\n\r\n\r\n\r\n<p>Pelaksanaan aturan baru memang pada awalnya menimbulkan banyak polemik karena masyarakat belum terbiasa. Peraturan tersebut dibuat demi mencoba dan mengetahui cara apa yang optimal untuk memajukan pendidikan Indonesia. Ini merupakan langkah awal yang baik dan memerlukan dukungan dari kita semua agar tujuan baik itu tercapai.</p>\r\n\r\n\r\n\r\n<p>Edufriends <em>nggak</em> perlu khawatir dengan sistem zonasi ini. Karena kita adalah tetap kita dimanapun berada. Jadi, meski kamu hanya bisa bersekolah di sekolah yang bukan unggulan, Edufriends tetap bisa mengukir prestasi <em>kok</em>. Apalagi ada Edutore yang bisa jadi teman belajar kamu dan mendukung meningkatnya nilai akademis kamu kan.</p>\r\n\r\n\r\n\r\n<p>Semangat terus ya, Edufriends!</p>\r\n\r\n\r\n\r\n<p>&nbsp;</p>\r\n\r\n\r\n\r\n<p>&nbsp;</p>\r\n<div style=\"text-align:left\" class=\"yasr-auto-insert-overall\"></div>",
        "protected": false
      },
      "excerpt": {
        "rendered": "<p>Pendaftaran Penerimaan Peserta Didik Baru (PPDB) telah dibuka. Kementerian Pendidikan dan Kebudayaan menetapkan aturan baru berupa sistem zonasi. Pemberlakuan sistem zonasi ini sempat bikin heboh pelajar juga orang tua murid. Lho emang ada apa sih dengan sistem zonasi? Sebelum kita &hellip; </p>\n",
        "protected": false
      },
      "author": 2,
      "featured_media": 742,
      "comment_status": "closed",
      "ping_status": "closed",
      "sticky": false,
      "template": "",
      "format": "standard",
      "meta": {
        "pmpro_default_level": 0,
        "yasr_overall_rating": 0,
        "yasr_auto_insert_disabled": "",
        "yasr_review_type": ""
      },
      "categories": [
        32
      ],
      "tags": [
        92,
        91,
        93,
        90
      ],
      "modified_by": "Feby",
      "_links": {
        "self": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/716"
          }
        ],
        "collection": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts"
          }
        ],
        "about": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/types/post"
          }
        ],
        "author": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/users/2"
          }
        ],
        "replies": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/comments?post=716"
          }
        ],
        "version-history": [
          {
            "count": 2,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/716/revisions"
          }
        ],
        "predecessor-version": [
          {
            "id": 819,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/716/revisions/819"
          }
        ],
        "wp:featuredmedia": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/media/742"
          }
        ],
        "wp:attachment": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/media?parent=716"
          }
        ],
        "wp:term": [
          {
            "taxonomy": "category",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/categories?post=716"
          },
          {
            "taxonomy": "post_tag",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/tags?post=716"
          }
        ],
        "curies": [
          {
            "name": "wp",
            "href": "https://api.w.org/{rel}",
            "templated": true
          }
        ]
      }
    },
    {
      "id": 714,
      "date": "2019-06-27T09:52:25",
      "date_gmt": "2019-06-27T02:52:25",
      "guid": {
        "rendered": "https://edutore.com/news/news/?p=714"
      },
      "modified": "2019-07-31T09:45:31",
      "modified_gmt": "2019-07-31T02:45:31",
      "slug": "pro-kontra-produk-ramah-lingkungan",
      "status": "publish",
      "type": "post",
      "link": "https://edutore.com/news/pro-kontra-produk-ramah-lingkungan/",
      "title": {
        "rendered": "Pro Kontra Produk Ramah Lingkungan"
      },
      "content": {
        "rendered": "\r\n<p>Edufriends, kamu tentu menyadari kalau beberapa pasar swalayan memberikan pilihan untuk menggunakan tas yang bisa dipakai ulang, atau restoran sudah tidak memberikan sedotan kepada para pelanggannya. </p>\r\n\r\n\r\n\r\n<p>Mengapa sih sampai dilakukan hal seperti itu?</p>\r\n\r\n\r\n\r\n<p>Pembatasan ini dilakukan untuk mengurangi penggunaan plastik yang kemudian menjadi limbah plastik yang sulit didaur ulang. Kamu juga udah tahu juga dong jumlah limbah plastik yang ada saat ini sulit banget dikontrol dan berdampak buruk pada semua aspek.</p>\r\n\r\n\r\n\r\n<p>Hal yang menjadi dilema kita semua adalah ingin mengurangi tapi kebutuhan kita dalam menggunakan plastik masih banyak banget. <em>Duh</em> gimana ya caranya?</p>\r\n\r\n\r\n\r\n<p>Eits, gampang banget kok Edufriends, untuk menyesuaikan diri dengan perubahan ini bisa dengan perlahan-lahan dan dimulai dari hal yang sederhana. Misalnya nih, menggunakan sedotan dan tas belanja yang dapat digunakan ulang. Sedotan yang dapat diapakai ulang umumnya dibuat dari besi namun ada material – material lain yang digunakan seperti kayu bambu, silikon, dan kaca. Tas belanja yang bisa dipakai berulang – ulang umumnya dibuat dari bahan plastik tetapi dirangkai agar memiliki kekuatan yang lebih atau dibuat dari bahan lain seperti kain dan kanvas agar memiliki kekuatan lebih.</p>\r\n\r\n\r\n\r\n<p>Tetapi menggunakan produk ramah lingkungan tersebut memiliki sisi positif dan negatif, apa aja ya? simak ulasan dari Eduteam yuk!</p>\r\n\r\n\r\n\r\n<p><strong>Pro</strong></p>\r\n\r\n\r\n\r\n<p>Produk  yang dapat digunakan berulang kali memiliki efek yang bagus pada lingkungan karena mengurangi jumlah plastik yang digunakan sekali kemudian dibuang, dan bahan yang bisa digunakan berkali-kali umumnya terbuat dari bahan yang mampu bertahan lama. Produk ramah lingkungan seperti sedotan dan tas juga mudah untuk dibersihkan, walaupun sedotan memerlukan sikat khusus untuk membersihkan bagian dalamnya.</p>\r\n\r\n\r\n\r\n<p><strong>Kontra</strong></p>\r\n\r\n\r\n\r\n<p>Walaupun produk ramah lingkungan dapat digunakan berulang kali. Proses pembuatannya menggunakan lebih banyak energi dan material daripada produk yang dibuat dari plastik. Proses pembuatan produk ramah lingkungan juga menghasilkan limbah, dimana jumlah limbahnya lebih banyak dari proses produksi produk plastik.</p>\r\n\r\n\r\n\r\n<p>Ohya, Edufriends, kita juga harus menyadari dan mengakui bahwa produk ramah lingkungan  juga menghasilkan limbah dalam proses produksinya, tetapi karena sifatnya yang bisa digunakan berulang kali seperti tas ramah lingkungan yang digunakan lebih dari 100 kali bisa memberikan perubahan kepada lingkungan yaitu mengurangi penumpukan sampah plastik.</p>\r\n\r\n\r\n\r\n<p>Hal demikian menunjukkan bahwa memang tidak mudah untuk lepas dari yang namanya menghasilkan limbah. Maka yang bisa kita lakukan adalah melakukan yang terbaik dan mencari cara yang dampak buruknya lebih sedikit dong Edufriends.</p>\r\n\r\n\r\n\r\n<p>Tetapi setelah mengetahui pro dan kontra berikut Edufriends tidak perlu khawatir dalam menggunakan produk plastik atau produk yang ramah lingkungan, karena yang penting adalah sikap kita yang mengutamakan lingkungan bukan menggunakan dan memiliki produk ramah lingkungan sebanyak – banyaknya.</p>\r\n\r\n\r\n\r\n<p>Jadi, apa langkah Edufriends dalam mengurangi penggunaan plastik?</p>\r\n<div style=\"text-align:left\" class=\"yasr-auto-insert-overall\"></div>",
        "protected": false
      },
      "excerpt": {
        "rendered": "<p>Edufriends, kamu tentu menyadari kalau beberapa pasar swalayan memberikan pilihan untuk menggunakan tas yang bisa dipakai ulang, atau restoran sudah tidak memberikan sedotan kepada para pelanggannya.  Mengapa sih sampai dilakukan hal seperti itu? Pembatasan ini dilakukan untuk mengurangi penggunaan plastik &hellip; </p>\n",
        "protected": false
      },
      "author": 1,
      "featured_media": 732,
      "comment_status": "closed",
      "ping_status": "closed",
      "sticky": false,
      "template": "",
      "format": "standard",
      "meta": {
        "pmpro_default_level": 0,
        "yasr_overall_rating": 0,
        "yasr_auto_insert_disabled": "",
        "yasr_review_type": "Other"
      },
      "categories": [
        35,
        40
      ],
      "tags": [],
      "modified_by": "Feby",
      "_links": {
        "self": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/714"
          }
        ],
        "collection": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/posts"
          }
        ],
        "about": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/types/post"
          }
        ],
        "author": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/users/1"
          }
        ],
        "replies": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/comments?post=714"
          }
        ],
        "version-history": [
          {
            "count": 2,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/714/revisions"
          }
        ],
        "predecessor-version": [
          {
            "id": 842,
            "href": "https://edutore.com/news/wp-json/wp/v2/posts/714/revisions/842"
          }
        ],
        "wp:featuredmedia": [
          {
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/media/732"
          }
        ],
        "wp:attachment": [
          {
            "href": "https://edutore.com/news/wp-json/wp/v2/media?parent=714"
          }
        ],
        "wp:term": [
          {
            "taxonomy": "category",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/categories?post=714"
          },
          {
            "taxonomy": "post_tag",
            "embeddable": true,
            "href": "https://edutore.com/news/wp-json/wp/v2/tags?post=714"
          }
        ],
        "curies": [
          {
            "name": "wp",
            "href": "https://api.w.org/{rel}",
            "templated": true
          }
        ]
      }
    }
  ]
}
