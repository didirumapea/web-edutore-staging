export default function ({ store, redirect, route }) {
    if (isLogin(store) && route.name === 'login') {
        // console.log(store)
      return redirect('/admin')
    }
  
    if (!isLogin(store) && isAdminRoute(route)) {
        // console.log(store)
      return redirect('/login')
    }
  }
  
  const isLogin = (store) => {
    // console.log(store)
    return (store && store.state && store.state.user)
  }
  
  const isAdminRoute = (route) => {
    //   console.log(route)
    if (route.matched.some(record => record.path === '/admin')) {
      return true
    }
  }