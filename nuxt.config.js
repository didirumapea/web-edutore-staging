const pkg = require('./package')
  const webpack = require("webpack");


module.exports = {
  mode: 'universal',
  // loading: '~/components/loading.vue',

  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: ' %s - Edutore by Gramedia',//pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Platform edukasi online dari Gramedia yang menyajikan berbagai Konten Edukasi Unggulan dari partner-partner pilihan.' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://use.fontawesome.com/releases/v5.7.1/css/all.css',
      },
      {
        rel: 'stylesheet',
        href:
          'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css',
      }
    ],
    script: [
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'},
      // { src: 'https://www.gstatic.com/firebasejs/5.4.0/firebase-app.js'},
      // { src: 'https://www.gstatic.com/firebasejs/5.4.0/firebase-database.js'},

      // { src: '/js/alexa-metrixs.js'}
      // { src: 'js/popper.min.js'}
    ],
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#557CBF', height: '3px' },
  // loading: [
  //   '~/components/loading.vue',
  //   { color: '#FF0000', height: '3px' }
  // ],

  /*
  ** Global CSS
  */
  css: [
    
    '~/assets/main.css',
    '@/assets/css/bootstrap.min.css',
    '@/assets/dist/css/swiper.min.css',
    '@/assets/dist/css/swiper.css',
    // '@/assets/css/bootstrap.css',
    // "@/node_modules/bootstrap/dist/css/bootstrap.css",
   
    '@/assets/css/styles.css',
    // '@/assets/css/modul_bariq.css',
    // '@/assets/css/fontawesome.css',
    // '@/assets/css/fa2.css',
    '@/assets/css/responsive.css',
  ],
  
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    // { src: '~plugins/countUp.js', ssr: false },
    '@/plugins/popper',
    '@/plugins/bootstrap',
    '~/plugins/vue-js-modal',
    '~/plugins/axios',
    { src: '~/plugins/swiper.js', ssr: false },
    '~/plugins/func',
    {src: '~plugins/vee-validate.js', ssr: true},
    {src: '~plugins/vue-pagination.js', ssr: false},
    {src: '~plugins/alexa-metrixs.js', ssr: false},
    // { src: '~/plugins/google-adsense', ssr: false }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // ['@nuxtjs/google-adsense']
    ['@nuxtjs/sitemap'],
    // Simple usage
    // 'nuxt-facebook-pixel-module',

    // With options
    ['nuxt-facebook-pixel-module', {
      /* module options */
      track: 'PageView',
      pixelId: '1355315167958492',
      disabled: false
    }],
    '@nuxtjs/axios',
    // '@nuxtjs/feed',
    // '@nuxtjs/auth',
    '@nuxtjs/toast',
    'cookie-universal-nuxt',
    ['@nuxtjs/google-tag-manager', {
      id: 'GTM-MS3M7VG',
      layer: 'dataLayer',
      pageTracking: true,
      dev: true, // set to false to disable in dev mode
      // query: {
      //   // query params...
      //   gtm_auth:        '...',
      //   gtm_preview:     '...',
      //   gtm_cookies_win: '...'
      // }
    }],
    // Simple usage
    // '@nuxtjs/proxy',

    // With options
    // ['@nuxtjs/proxy', { pathRewrite: { '^/api' : '/api/v1' } }]
  ],
  sitemap: {
    path: '/sitemapx.xml',
    hostname: 'https://edutore.com',
    defaults: {
      changefreq: 'daily',
      priority: 1,
      lastmod: new Date(),
      lastmodrealtime: true
    }
  },
  // 'google-adsense': {
  //   id: 'ca-pub-2311798948770569',
  // },
  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:3000',
    API_URL: 'https://api.edutore.com/api/v1'
  },
  // feed: [
  //   // A default feed configuration object
  //   {
  //     path: 'https://edutore.com/news/feed/', // The route to your feed.
  //     async create (feed) {
  //       feed.options = {
  //         title: 'Edutore Blog',
  //         link: `https://edutore.com/news/feed/`,
  //         description: "This is Edutore personal feed!"
  //       }
  //
  //       const posts = await client.getEntries({
  //         'content_type': process.env.CTF_BLOG_POST_TYPE_ID
  //       })
  //
  //       posts.items.forEach(post => {
  //         feed.addItem({
  //           title: post.fields.title,
  //           id: post.fields.slug,
  //           link: `${process.env.BASE_URL}/posts/${post.fields.slug}`,
  //           description: post.fields.description,
  //           content: post.fields.description,
  //           date: new Date(post.fields.publishDate),
  //           image: post.fields.heroImage.fields.file.url
  //         })
  //       })
  //
  //       feed.addCategory('Tech')
  //
  //       feed.addContributor({
  //         name: 'Nakamu',
  //         email: 'yuuki.nakamura.0828@gmail.com',
  //         link: process.env.BASE_URL
  //       })
  //     }, // The create function (see below)
  //     cacheTime: 1000 * 60 * 15, // How long should the feed be cached
  //     type: 'rss2', // Can be: rss2, atom1, json1
  //     data: ['Some additional data'] //will be passed as 2nd argument to `create` function
  //   }
  // ],
  axios: {
    // ---------- maunual setting ---------
    // proxyHeaders: false,
    // credentials: false,
    // baseURL: 'http://api.edutore.com:3000/api/v1/'
    // baseURL: 'https://api.edutore.com/api/v1/',
    // baseURL: 'https://10.148.0.6:3000/api/v1/'
    baseURL: process.env.NODE_ENV !== "production"
              ? `https://api.edutore.com/api/v1/`
              : "https://api.edutore.com/api/v1/",
    // ---------- proxy setting -----------
    // proxy: true, // Can be also an object with default options
    // prefix: '/api/'
  },
  proxy: {
    '/api/': {
      target: 'https://api.edutore.com/api/v1',
      pathRewrite: {'^/api/': ''}
    }
  },
  /*
  ** Nuxt.js toast
  */
 toast: {
  position: 'bottom-right',
  duration: 3000
},

  server: {
    port: 8000, // default: 3000
    host: '0.0.0.0', // default: localhost,
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    // vendor: ["jquery", "bootstrap"],
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery"
      })
    ],
    // extend(config, { isDev, isClient }) {
    //   if (isDev && isClient) {
    //     config.module.rules.push({
    //       enforce: "pre",
    //       test: /\.(js|vue)$/,
    //       // loader: "eslint-loader",
    //       exclude: /(node_modules)/
    //     });
    //   }
    // }
    extend(config, ctx) {
      
    }
  },
  // router: {
  //   middleware: ['router-auth']
  // },
}
