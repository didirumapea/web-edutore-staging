// STATE AS VARIABLE
export const state = () => ({
    mySubs: [],
    myListModuleSubs: [],
    myListModulesSubsId: []
  })
// ACTIONS AS METHODS
  export const actions = { // asyncronous
    async getListLangganan ({ commit }) {
        return await this.$axios.get('subscriptions')
        .then((response) => {
            // console.log(response)
            // commit('set_user', response.data.user)
            return response
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          // console.log(err.response)
          return err.response
      })
      },
    async getDetailLangganan ({ commit }, payload) {
      return await this.$axios.get('subscriptions/'+payload)
        .then((response) => {
          // console.log(response)
          // commit('set_user', response.data.user)
          return response
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          // console.log(err.response)
          return err.response
        })
    },
    async setMySubscriptions ({ commit }) {
      if (this.$cookies.get('user').token !== undefined){
        let token = this.$cookies.get('user').token
        return await this.$axios.$get('my-subscriptions', {headers : {'x-access-token': token}})
          .then((response) => {
            // console.log(response)
            commit('setMySubscriptions', response.data.rows)
            return response
          })
          .catch(err => {
            // alert(JSON.stringify(err.response))
            console.log(err.response)
            return err.response
            //   return err.response
          })
      }
    },
    async getMyModuleSubscriptions ({ commit }, payload) {
      if (this.$cookies.get('user').token !== undefined){
        let token = this.$cookies.get('user').token
        return await this.$axios.$get('my-subscriptions/'+payload, {headers : {'x-access-token': token}})
          .then((response) => {
            // console.log(response.data.paket_langganan.modules_langganan)
            commit('setMyListSubscriptions', response)
            return response
          })
          .catch(err => {
            // alert(JSON.stringify(err.response))
            console.log(err.response)
            return err.response
            //   return err.response
          })
      }
    },
  }
// MUTATION AS LOGIC
export const mutations = { // syncronous
  clearMyListSubs(state){
    state.mySubs = []
    state.myListModuleSubs = []
    this.$cookies.remove('my-subscriptions')
    this.$cookies.remove('my-list-subscriptions')
  },
  getMySubscription(state, payload){
    // console.log(payload)
    if (this.$cookies.get('my-subscriptions') !== undefined){

    }
    // console.log(this.$cookies.get('my-subscriptions'))
  },
  getMyListSubscription(state){
    // console.log(payload)
    if (this.$cookies.get('my-list-subscriptions') !== undefined){
      state.myListModuleSubs = this.$cookies.get('my-list-subscriptions')
    }
    // console.log(this.$cookies.get('my-list-subscriptions'))
  },
  setMySubscriptions(state, payload){
    // console.log(payload)
    payload.forEach(element => {
      let data = {
        id: element.id,
        userId: element.user_id
      }
          state.mySubs.push(data)
      // console.log(element)
    })
    this.$cookies.set('my-subscriptions', state.mySubs, {
        path: '/',
        maxAge: 60 * 60 * 24 * 7
      })
    // console.log(this.$cookies.get('my-subscriptions'))
  },
  setMyListSubscriptions(state, payload){
    // console.log(payload)
    let subsId = payload.data.id
    let moduleSubs = payload.data.paket_langganan
    let data = {
      status_expire: payload.status_expire,
      idSubsModule: 0,
      listSubsModule: [],
    }
    data.idSubsModule = subsId
    moduleSubs.modules_langganan.forEach(element => {
      data.listSubsModule.push(element.modules.id)
      // console.log(element.modules.id)
    })
    state.myListModuleSubs.push(data)
    // console.log(state.myListModuleSubs)
    this.$cookies.set('my-list-subscriptions', state.myListModuleSubs, {
      path: '/',
      maxAge: 60 * 60 * 24 * 7
    })
    // console.log(state.myListModuleSubs)
    // state.myListModuleSubs += payload
    // this.$cookies.set('my-module', payload, {
    //     path: '/',
    //     maxAge: 60 * 60 * 24 * 7
    //   })
  },
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
  mySubs: state => {
    return state.mySubs
  },
  myListModuleSubs: state => {
    return state.myListModuleSubs
  },
  myListModulesSubsId: state => {
    return state.myListModulesSubsId
  }
}
