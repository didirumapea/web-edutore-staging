const state = () => ({
    list: [],
    image: 'this is image',
    product: []
  })
  
const mutations = {
    add (state, text) {
        state.list.push({
        text: text,
        done: false
        })
    },
    remove (state, { todo }) {
        state.list.splice(state.list.indexOf(todo), 1)
    },
    toggle (state, todo) {
        todo.done = !todo.done
    }
}

const getters = {
    image: state => {
		return state.image
    },
    product: state => {
		return state.product
	}
}

export default {
    state,
    mutations,
    getters
}