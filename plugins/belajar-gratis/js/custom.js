
$(function() {
	window.scrollReveal = new scrollReveal();
	"use strict";
  console.log('from before custom js')
  // Backstretchs
  // $("#header").backstretch("~/assets/img/belajar-gratis/3.jpg");
  // $("#services").backstretch("../../../assets/img/belajar-gratis/3.jpg");
  $("#header").backstretch(require('~/assets/img/belajar-gratis/3.jpg'));
  $("#services").backstretch(require('~/assets/img/belajar-gratis/3.jpg'));
  console.log('from after custom js')
	// PreLoader
	$(window).load(function() {
		$(".loader").fadeOut(400);
	});


	// Countdown
	$('.countdown').downCount({
		date: '12/12/2014 12:00:00',
		offset: +10
	});			
    
});
